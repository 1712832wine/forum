<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppLayout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public $search = '';
    public $click = 0;
    public function updateSearch(){
        return dd($this->search);
        $this->emit('updateSearch',$search);
    }
    public function click(){
        $this->click+=1;
    }
    public function render()
    {
        return view('layouts.app');
    }
}
