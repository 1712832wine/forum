<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadsController extends Controller
{
    public function download($filename) {
        return response()->download('storage/'.$filename);
    }
}
