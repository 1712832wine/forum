<?php

namespace App\Http\Livewire;

use File;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

use Livewire\Component;
use App\Models\Post;
use App\Models\Comment;
use App\Models\User;


class Personal extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    protected $listeners = ['updateSearch',
    'refreshComponent' => '$refresh',
    'updatedStatus',
    'updatedCategory',
    'Post:delete' => 'delete',
    'Personal:redirect' => 'Credirect'];
    
    public $search;
    public $status, $category;
    public User $user;

    public function mount($id) {
        $this->user = User::findOrFail($id);
    }


    public function updatedCategory($category){
        $this->category = $category;
        $this->resetPage();
    }
    public function updatedStatus($status){
        $this->status = $status;
        $this->resetPage();
    }
    public function updateSearch($search){
        $this->search = $search;
        $this->resetPage();
    }

    public function Credirect(){
        return redirect()->route('login');
    }

    public function editPost($id){
        $this->emit('edit',$id);
        $this->dispatchBrowserEvent('openModal');
    }

    public function delete($id) {
        $imgs = json_decode(Post::find($id)['images']);
        foreach($imgs as $img) {
            if (File::exists(public_path('storage/'.$img))) {
                File::delete(public_path('storage/'.$img));
            }
        }
        Comment::where('belongto', $id)->delete();
        Post::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Post:delete',
            'params'      => $id,
        ]);
    }
    public function export($file)
    {
        if ($file!==''){
            return response()->download('storage/'.$file);;
        } else{
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => "Don't have file!",
                'text'    => "Don't have any file here",
            ]);
        }
    }
    public function follow(){
        if (Auth::check()) {
            if (Auth::id()===$this->user->id)
            {
                $this->emit('swal:modal', [
                    'type'    => 'warning',
                    'icon'    => 'warning',
                    'title'   => 'You cannot follow yourself!',
                ]);
            }
            else
            {
                $followings = json_decode(Auth::user()->followings);
                $followers = json_decode($this->user->followers);
                if (!$followings){
                    $followings = [];
                }
                if (!$followers){
                    $followers = [];
                }
                // Following
                if (in_array($this->user->id,$followings)){
                    unset( $followings[array_search( $this->user->id, $followings )] );
                } else {
                    array_push($followings, $this->user->id);
                }
                // follower
                if (in_array(Auth::id(),$followers)){
                    unset( $followers[array_search( Auth::id(), $followers )] );
                } else{
                    array_push($followers, Auth::id());
                }
                // following
                $user = Auth::user();
                $user->followings = json_encode(array_values($followings));
                $user->save();
                // follower
                $this->user->followers = json_encode(array_values($followers));
                $this->user->save();
                
            }
        } else
        {
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'You have not login',
                'text'        => "You have to login to do this action",
                'confirmText' => 'Go to login page',
                'method'      => 'Personal:redirect',
            ]);
        }
    }


    public function render()
    {
        $posts = Post::where([['title', 'like', '%'.$this->search.'%'],
        ['category','like','%'.$this->category.'%'],
        ['sharewith','like','%PUBLISH%'],
        ['status','not like','%Pending%'],
        ['status','like','%'.$this->status.'%'],
        ['author',$this->user->id],
        ])->orderBy('updated_at','desc')->paginate();
        
        return view('livewire.personal',[
            'posts' => $posts
        ])->layout('layouts.profile');
    }
}
