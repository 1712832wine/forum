<?php

namespace App\Http\Livewire;


use Livewire\Component;
use App\Models\Post;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class DashboardGetPosts extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['updateSearch', 'refreshComponent' => '$refresh','Post:delete' => 'delete',];
    public $search, $post_id = 0;
    public $type; 
    public function updateSearch($search){
        $this->search = $search;
        $this->resetPage();
    }
    public function render()
    {
        switch ($this->type) {
            case 1:
                // posts of yours
                $posts = Post::where([['title', 'like', '%'.$this->search.'%'],['author',Auth::id()]])->paginate(5);
                break;
            case 2:
                // posts of all users
                $posts = Post::where([['title', 'like', '%'.$this->search.'%'],['author',Auth::id()]])->paginate(5);
                break;       
            default:
            $posts = Post::where([['title', 'like', '%'.$this->search.'%'],['author',Auth::id()]])->paginate(5);
                break;
        }
        
        return view('livewire.dashboard-get-posts',['posts' => $posts]);
    }
    public function delete($id) {
        Post::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Post:delete',
            'params'      => $id,
        ]);
    }
}
