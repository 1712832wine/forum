<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Collection;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use File;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;

use Pusher\Pusher;
use Notification;
use App\Notifications\PostNotification;

class PendingPosts extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = [
        'updateSearch',
        'refreshComponent' => '$refresh',
        'Post:delete' => 'delete',
    ];
    public $search = '', $post_id = 0;
    public Post $post;
    public $data = [];
    protected $rules = [
        'post.status' => 'required',
    ];
    // SAVE
    public function saveChange() {
        $this->validate();
        $this->post->save();
        
        if ($this->post->status!=='Pending'){
            // thông báo khi bài viết được kiểm duyệt
            // lấy ra tác giả bài viết
            
            $author = User::findOrFail($this->post->author);
            //lấy ra danh sách những người follow tác giả
            $for = User::whereIn('id', json_decode($author->followers))->get();
            
            $data = [
                'message' => "has created a new post",
                'author' => $this->post->author,
                'link' => route('post_detail',$this->post->id),
            ];
           
            Notification::send($for, new PostNotification($data));

            $options = array(
                'cluster' => 'ap1',
                'encrypted' => true
            );

            $pusher = new Pusher(
                env('PUSHER_APP_KEY'),
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'),
                $options
            );

            $pusher->trigger('NotificationEvent', 'send-message', $data);
        }
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Change Status Success!',
            'text'    => "The item has been change status successfully",
        ]);
        $this->resetField();
        $this->dispatchBrowserEvent('closeModalPendingPosts');
    }

    public function mount(){
        $this->post = new Post;
    }

    public function updateSearch($search){
        $this->search = $search;
        $this->resetPage();
    }

    public function resetField(){
        $this->post = new Post;
    }

    public function changeStatus($id) {
        $this->authorize('update',Post::find($id));
        $this->resetField();
        $this->post = Post::findOrFail($id);
        $this->dispatchBrowserEvent('openModalPendingPosts');
        // $this->emit('edit', $id);
    }

    public function delete($id) {
        $imgs = json_decode(Post::find($id)['images']);
        foreach($imgs as $img) {
            if (File::exists(public_path('storage/'.$img))) {
                File::delete(public_path('storage/'.$img));
            }
        }
        Comment::where('belongto', $id)->delete();
        Post::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Post:delete',
            'params'      => $id,
        ]);
    }
    public function render()
    {
        $this->authorize('view',Auth::user());
        $posts = Post::where([['status', 'Pending'],['title', 'like', '%'.$this->search.'%']])->orderBy('updated_at','asc')->paginate(5);
        return view('livewire.pending-posts',['posts' => $posts]);
    }
}
