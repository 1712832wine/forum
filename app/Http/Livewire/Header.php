<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;

use Livewire\Component;
use App\Models\Post;
class Header extends Component
{
    public $search = '';
    protected $listeners = ['refreshNotification'=>'$refresh'];
    public function updatedSearch()
    {
        $this->emit('updateSearch', $this->search);
    }
    public function render()
    {
        $count =  count(Post::where([['status', 'Pending']])->get());
        return view('livewire.header',[
            'count' => $count,]);
    }
}
