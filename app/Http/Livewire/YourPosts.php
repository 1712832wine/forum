<?php

namespace App\Http\Livewire;
use File;
use Livewire\Component;

use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\Comment;

class YourPosts extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = [
    'updateSearch', 
    'refreshComponent' => '$refresh',
    'Post:delete' => 'delete',];
    public $search, $post_id = 0;
    public $type; 
    public function updateSearch($search){
        $this->search = $search;
        $this->resetPage();
    }
    public function edit($id) {
        $this->emit('edit', $id);
    }
    public function openModal(){
        $this->emit('openModal');
    }
    public function delete($id) {
        $imgs = json_decode(Post::find($id)['images']);
        foreach($imgs as $img) {
            if (File::exists(public_path('storage/'.$img))) {
                File::delete(public_path('storage/'.$img));
            }
        }
        Comment::where('belongto', $id)->delete();
        Post::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Post:delete',
            'params'      => $id,
        ]);
    }
    public function render()
    {
        $posts = Post::where([['title', 'like', '%'.$this->search.'%'],['author',Auth::id()]])->orderBy('created_at','desc')->paginate(5);
        return view('livewire.your-posts',['posts' => $posts]);
    }
}
