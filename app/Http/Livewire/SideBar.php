<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SideBar extends Component
{

    public $status;
    public $category;

    public function openCreatePost(){
        if (Auth::check()) {
            $this->emit('resetField');
            $this->dispatchBrowserEvent('openModal');
        } else
        {
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Create Post Failed',
                'text'        => "You have not login, so you can not create a post, please login first?",
                'confirmText' => 'Go to login page',
                'method'      => 'ModalPost:redirect',
            ]);
        }

    }
    public function render()
    {
        return view('livewire.side-bar');
    }
    public function updatedCategory() {
        $this->emit('updatedCategory',$this->category);
    }
    public function updatedStatus() {
        $this->emit('updatedStatus',$this->status);
    }
    public function getAuthID() {
        $this->emit('authId',Auth::user()->id);
    }
}
