<?php

namespace App\Http\Livewire;

use Livewire\WithPagination;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\Status;

class StatusComponent extends Component
{
    use AuthorizesRequests;
    use WithPagination;

    public Status $status;
    public $isTemplate = true;
    public $templates = ['primary','secondary','success','danger','warning','info','light','dark'];
    protected $paginationTheme = 'bootstrap';
    protected $rules = [
        'status.title' => 'required',
        'status.background'=> '',
        'status.color'=>''
    ];
    protected $listeners = [
        'Status:delete' => 'delete',
    ];
    public function mount(){
        $this->status = new Status;
        $this->status->background = 'primary';
    }

    public function isTemplate($value){
        return in_array($value, $this->templates);
    }

    public function changeTemplate($id){
        $this->status->background = $this->templates[$id];
    }

    

    public function render()
    {
        $this->authorize('view',Auth::user());
        $statuses = Status::paginate(5);
        return view('livewire.status-component',['statuses' => $statuses]);
    }
    
    // STATUS
    public function resetField(){
        $this->status = new Status;
        $this->status->background = 'primary';
    }
    // CREATE 
    public function toggleCreate(){
        $this->authorize('create',Status::class);
        $this->resetField();
        $this->dispatchBrowserEvent('openModalStatus');
    }
    // EDIT
    public function openEdit($id){
        $this->authorize('update',Status::find($id));
        $this->resetField();
        $this->status = Status::findOrFail($id);
        $this->dispatchBrowserEvent('openModalStatus');
    }
    // SAVE
    public function saveChange() {
        $this->validate();
        $this->status->save();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Add Success!',
            'text'    => "The item has been added successfully",
        ]);
        $this->resetField();
        $this->dispatchBrowserEvent('closeModalStatus');
    }
    public function delete($id) {
        Status::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->authorize('delete',Status::find($id));
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Status:delete',
            'params'      => $id,
        ]);
    }
}
