<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\File;

use Livewire\WithPagination;

use App\Models\Post;
use App\Models\Comment;
use App\Models\User;

//Notification added

use Pusher\Pusher;
use Notification;
use App\Notifications\PostNotification;

class PostDetail extends Component
{
    use AuthorizesRequests;
    use WithFileUploads;
    use WithPagination;
    public Post $post;
    public Comment $comment;
    public User $author;
    public $search = '';
    public $images = [];
    public $commentId = [];
    protected $paginationTheme = 'bootstrap';
    protected $listeners = [
        'updateSearch',
        'PostDetail:redirect' => 'CRedirect',
        'Post:delete' => 'deletePost',
        'Comment:delete' => 'deleteComment',
        'Comment:pin' => 'pinComment',
        'refreshComponent' => '$refresh',
    ];
    protected $rules = ['comment.content' => 'required|string|max:500',];

    public function updateSearch($search)
    {
        $this->search = $search;
        $this->resetPage();
    }


    public function mount($id = "0")
    {
        $this->post = Post::findOrFail($id);
        $this->author = User::findOrFail($this->post->author);
        $this->comment = new Comment;
        $this->comment->like = json_encode([]);
    }


    public function CRedirect()
    {
        return redirect('login');
    }

    public function resetField()
    {
        $this->comment = new Comment;
        $this->dispatchBrowserEvent('refreshEditorCMT');
    }


    public function submitComment()
    {
        // return dd($this->comment);
        if (Auth::check()) {
            $this->validate();
            if (!$this->comment->like) $this->comment->like = json_encode([]);
            //thông báo khi bài viết được comment
            //get comment user
            $for = User::findOrFail($this->post->author);
            if ($for->id !== Auth::id()) {
                $data = [
                    'message' => "has commented on your post",
                    'author' => Auth::id(),
                    'link' => route('post_detail', $this->post->id),
                ];
                //
                Notification::send($for, new PostNotification($data));
                //

                $options = array(
                    'cluster' => 'ap1',
                    'encrypted' => true
                );

                $pusher = new Pusher(
                    env('PUSHER_APP_KEY'),
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'),
                    $options
                );

                $pusher->trigger('NotificationEvent', 'send-message', $data);
            }
            // end thông báo khi bài viết được comment
            $this->comment->author = Auth::id();
            $this->comment->belongto = $this->post->id;
            $images = [];
            foreach ($this->images as $image) {
                $name = md5($image . microtime()) . '.' . $image->extension();
                $image->storeAs('public', $name);
                array_push($images, $name);
            }

            $this->comment->images = json_encode($images);
            $this->comment->save();
            $this->resetField();
            $this->dispatchBrowserEvent('remove-images-n');
            $this->emit('swal:modal', [
                'type'    => 'success',
                'icon'    => 'success',
                'title'   => 'Comment Success!',
                'text'    => "You have comment successfully",
            ]);
        } else {
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'You have not logion',
                'text'        => "You have to login to do this action",
                'confirmText' => 'Go to login page',
                'method'      => 'PostDetail:redirect',
            ]);
        }
    }


    public function render()
    {
        // return dd($this->post);
        // $this->authorize('viewDetail', $this->post);
        $comments = Comment::where([
            ['belongto', $this->post->id],
            ['content', 'like', '%' . $this->search . '%']
        ])->orderBy('priority', 'desc')->paginate(10);
        return view('livewire.post-detail', ['comments' => $comments])->layout('layouts.forum');
    }


    public function showLike($id)
    {
        $this->commentId = json_decode(Comment::findOrFail($id)->like);

        $this->dispatchBrowserEvent('openModalcmt', $this->commentId);
    }

    public function handleLikeComment($id)
    {
        if (Auth::check()) {
            $this->comment = Comment::findOrFail($id);
            $vote = json_decode($this->comment->like);
            if (!$vote) {
                $vote = [];
            }
            if (in_array(Auth::id(), $vote)) {
                unset($vote[array_search(Auth::id(), $vote)]);
            } else {
                array_push($vote, Auth::id());
            }
            $this->comment->like = json_encode(array_values($vote));
            $this->comment->save();
            $this->comment = new Comment();
        } else {
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'You have not login',
                'text'        => "You have to login to do this action",
                'confirmText' => 'Go to login page',
                'method'      => 'PostDetail:redirect',
            ]);
        }
    }
    public function handleLike()
    {
        if (Auth::check()) {
            $vote = json_decode($this->post->votetotal);
            if (!$vote) {
                $vote = [];
            }
            if (in_array(Auth::id(), $vote)) {
                unset($vote[array_search(Auth::id(), $vote)]);
            } else {
                array_push($vote, Auth::id());
                //thông báo khi bài viết được comment
                //get comment user
                $for = User::findOrFail($this->post->author);
                if ($for->id !== Auth::id()) {
                    $data = [
                        'message' => "has liked on your post",
                        'author' => Auth::id(),
                        'link' => route('post_detail', $this->post->id),
                    ];
                    Notification::send($for, new PostNotification($data));
                    $options = array(
                        'cluster' => 'ap1',
                        'encrypted' => true
                    );

                    $pusher = new Pusher(
                        env('PUSHER_APP_KEY'),
                        env('PUSHER_APP_SECRET'),
                        env('PUSHER_APP_ID'),
                        $options
                    );

                    $pusher->trigger('NotificationEvent', 'send-message', $data);
                }
                // end thông báo khi bài viết được comment

            }
            $this->post->votetotal = json_encode(array_values($vote));
            $this->post->save();
        } else {
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'You have not login',
                'text'        => "You have to login to do this action",
                'confirmText' => 'Go to login page',
                'method'      => 'PostDetail:redirect',
            ]);
        }
    }
    // SETTING

    public function editPost($id)
    {
        $this->emit('edit', $id);
        $this->dispatchBrowserEvent('openModal');
    }

    public function deletePost($id)
    {
        $imgs = json_decode(Post::find($id)['images']);
        foreach ($imgs as $img) {
            if (File::exists(public_path('storage/' . $img))) {
                File::delete(public_path('storage/' . $img));
            }
        }
        Comment::where('belongto', $id)->delete();
        Post::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Post:delete',
            'params'      => $id,
        ]);
    }
    public function deleteComment($id)
    {
        $imgs = json_decode(Comment::find($id)['images']);
        foreach ($imgs as $img) {
            if (File::exists(public_path('storage/' . $img))) {
                File::delete(public_path('storage/' . $img));
            }
        }
        Comment::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDeleteComment($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Comment:delete',
            'params'      => $id,
        ]);
    }
    public function pinComment($id)
    {
        $comment = Comment::findOrFail($id);
        if ($comment->priority === 1) $comment->priority = null;
        else $comment->priority = 1;
        $comment->save();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Pin Success!',
            'text'    => "The item has been pinned successfully",
        ]);
    }
    public function confirmPinComment($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to do this Action?",
            'confirmText' => 'Pin',
            'method'      => 'Comment:pin',
            'params'      => $id,
        ]);
    }
}
