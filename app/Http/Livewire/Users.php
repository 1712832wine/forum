<?php

namespace App\Http\Livewire;
use App\Models\User;
use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
class Users extends Component
{
    use AuthorizesRequests;
    public User $user; 
    protected $listeners = ['User:delete' => 'delete',];
    protected $rules = [
        'user.role' => 'required',
    ];
    public function mount(){
        $this->user = new User;
    }

    public function render()
    {
        $this->authorize('view',Auth::user());
        $users = User::all();
        return view('livewire.users',['users' => $users]);
    }
    public function delete($id) {
        User::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->authorize('delete',User::find($id));
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'User:delete',
            'params'      => $id,
        ]);
    }
    public function openModal($id){
        $this->authorize('update',User::find($id));
        $this->user = User::find($id);
        $this->dispatchBrowserEvent('openModalUserRole');
    }
    public function saveChange() {
        $this->validate();
        // return dd($this->user->role);
        $this->user->save();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Edit Success!',
            'text'    => "The item has been edited successfully",
        ]);
        $this->dispatchBrowserEvent('closeModalUserRole');
    }

}
