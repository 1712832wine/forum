<?php

namespace App\Http\Livewire;

use Livewire\Component;

use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\Tag;
use App\Models\Post;
use App\Models\Category;

class ModalPost extends Component
{
    use WithFileUploads;
    public Post $post;
    public $images = [];
    protected $listeners = [
        'ModalPost:redirect' => 'CRedirect',
        'edit' => 'edit',
        'resetField',
        'openModal'
    ];
    public function mount()
    {
        $this->post = new Post;
        if (Category::first()) $this->post->category = Category::first()->id;
        $this->post->sharewith = 'PUBLISH';
    }

    protected $rules = [
        'post.title' => 'required|string|min:6',
        'post.category' => 'required',
        'post.content' => 'required|string|max:500',
        'post.sharewith' => 'required',
        'post.tags' => '',
    ];

    public function edit($id){
        // return dd('emit');
        $this->post = Post::findOrFail($id);
        $this->dispatchBrowserEvent('syncSelect2',json_decode($this->post->tags));
        $this->dispatchBrowserEvent('syncEditor',$this->post->content);
        $this->dispatchBrowserEvent('openModal');
    }
    public function resetField(){
        $this->emit('refreshComponent');
        $this->images = [];
        $this->post = new Post;
        if (Category::first()) $this->post->category = Category::first()->id;
        $this->post->sharewith = 'PUBLISH';
        $this->dispatchBrowserEvent('refreshTags');
        $this->dispatchBrowserEvent('refreshContent');
    }
    public function openModal(){
        $this->resetField();
        $this->dispatchBrowserEvent('openModal');
    }
    public function CRedirect(){
        return redirect('login');
    }

    public function saveAndBack(){
        $this->validate();
        $this->post->author = Auth::id();
        foreach ($this->post->tags as $tag) {
            if (!Tag::where('title', $tag)->exists()){
                $newTag = new Tag;
                $newTag->title = $tag;
                $newTag->save();
            }
        }
        $this->post->tags = json_encode($this->post->tags);
        $this->post->status = 'Pending';
        if (!$this->post->votetotal) $this->post->votetotal = json_encode([]);
        $images = [];
         // EXIST
         if ($this->post->images){
            foreach (json_decode($this->post->images) as $item_cur){
                array_push($images, $item_cur);
            }
        }
        // ADD
        foreach ($this->images as $image) {
            $name = md5($image . microtime()).'.'.$image->extension();
            $image->storeAs('public',$name);
            array_push($images, $name);
        }

        $this->post->images = json_encode($images);
        $this->post->save();
        $this->resetField();
        $this->dispatchBrowserEvent('remove-images');
        $this->dispatchBrowserEvent('closeModal');
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Create Post Success!',
            'text'    => "You have create post successfully, Please wait for censorship.",
        ]);
    }


    public function render()
    {
        return view('livewire.modal-post');
    }
}
