<?php

namespace App\Http\Livewire;

use File;
use Livewire\Component;
use Livewire\WithPagination;

use App\Models\Post;
use App\Models\Comment;

class GetPosts extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['updateSearch',
     'refreshComponent' => '$refresh',
    'updatedStatus',
    'updatedCategory',
    'Post:delete' => 'delete'];
    public $search, $post_id = 0;
    public $status, $category;

    public function updatedCategory($category){
        $this->category = $category;
        $this->resetPage();
    }
    public function updatedStatus($status){
        $this->status = $status;
        $this->resetPage();
    }
    public function updateSearch($search){
        $this->search = $search;
        $this->resetPage();
    }
    public function editPost($id){
        $this->emit('edit',$id);
        
    }

    public function delete($id) {
        $imgs = json_decode(Post::find($id)['images']);
        foreach($imgs as $img) {
            if (File::exists(public_path('storage/'.$img))) {
                File::delete(public_path('storage/'.$img));
            }
        }
        Comment::where('belongto', $id)->delete();
        Post::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Post:delete',
            'params'      => $id,
        ]);
    }
    public function export($file)
    {
        if ($file!==''){
            return response()->download('storage/'.$file);;
        } else{
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => "Don't have file!",
                'text'    => "Don't have any file here",
            ]);
        }
    }

    public function render(){
        $posts = Post::where([['title', 'like', '%'.$this->search.'%'],
        ['category','like','%'.$this->category.'%'],
        ['sharewith','like','%PUBLISH%'],
        ['status','not like','%Pending%'],
        ['status','like','%'.$this->status.'%'],
        ])->orderBy('updated_at','desc')->paginate(5);
        
        return view('livewire.get-posts',[
            'posts' => $posts
        ])->layout('layouts.forum');
    }
}
