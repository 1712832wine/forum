<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CategoryComponent extends Component
{
    use AuthorizesRequests;
    use WithPagination;

    public Category $category;
    public $isTemplate = true;
    public $templates = ['primary','secondary','success','danger','warning','info','light','dark'];
    protected $paginationTheme = 'bootstrap';
    protected $rules = [
        'category.title' => 'required',
        'category.background' => '',
        'category.color' => '',
    ];
    protected $listeners = [
        'Category:delete' => 'delete',
    ];

    public function isTemplate($value){
        return in_array($value, $this->templates);
    }

    public function changeTemplate($id){
        $this->category->background = $this->templates[$id];
    }

    public function mount(){
        $this->category = new Category;
        $this->category->background = 'primary';
    }

    public function render()
    {
        $this->authorize('view',Auth::user());
        $categories = Category::paginate(10);
        return view('livewire.category-component',['categories' => $categories]);
    }
    // CATEGORY
    public function resetField(){
        $this->category = new Category;
        $this->category->background = 'primary';
    }
    // CREATE 
    public function toggleCreate(){
        $this->authorize('create',Category::class);
        $this->resetField();
        $this->dispatchBrowserEvent('openModalCategory');
    }
    // EDIT
    public function openEdit($id){
        $this->authorize('update',Category::find($id));
        $this->resetField();
        $this->category = Category::findOrFail($id);
        $this->dispatchBrowserEvent('openModalCategory');
    }
    // SAVE
    public function saveChange() {
        $this->validate();
        $this->category->save();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Add Success!',
            'text'    => "The item has been added successfully",
        ]);
        $this->resetField();
        $this->dispatchBrowserEvent('closeModalCategory');
    }
    public function delete($id) {
        Category::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->authorize('delete',Category::find($id));
        $this->emit("swal:confirm",[
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Category:delete',
            'params'      => $id,
        ]);
    }
}
