<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Post;

class CheckPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        $params = $request->route()->parameters();
        $id = $params['id'];
        $post = Post::findOrFail($id);
        
        if ($post->status!=='Pending' && $post->sharewith==='PUBLISH')
        {
            return $next($request);
        }
        else {
            abort(404);
        };
    }
}
