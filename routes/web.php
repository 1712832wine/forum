<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\GetPosts;
use App\Http\Livewire\PostDetail;
use App\Http\Livewire\YourPosts;
use App\Http\Livewire\Users;
use App\Http\Livewire\Categories;
use App\Http\Livewire\StatusComponent;
use App\Http\Livewire\CategoryComponent;
use App\Http\Livewire\PostsOfUser;
use App\Http\Livewire\PendingPosts;
use App\Http\Livewire\Personal;
use App\Http\Controllers\DownloadsController;
use App\Http\Controllers\NotificationController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/forum/all');
});
Route::prefix('/forum')->group(function () {
    Route::get('/all', GetPosts::class)->name('all');
    Route::get('/profile/{id}', Personal::class)->name('profile');
    // Route::get('/popular_this_week', GetPosts::class)->name('popular_this_week');
    // Route::get('/popular_all_time', GetPosts::class)->name('popular_all_time');
    // Route::get('/solved', GetPosts::class)->name('solved');
    // Route::get('/unsolved', GetPosts::class)->name('unsolved');
    // Route::get('/no_replied_yet', GetPosts::class)->name('no_replied_yet');
    Route::get('/post_detail/{id}', PostDetail::class)->name('post_detail')->middleware('checkpost');
});



Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/your_posts', YourPosts::class)->name('your_posts');
    Route::get('/users', Users::class)->name('users');
    Route::get('/category', CategoryComponent::class)->name('categories');
    Route::get('/status', StatusComponent::class)->name('status');
    Route::get('/pending_posts', PendingPosts::class)->name('pending_posts');

});

Route::get('/download/{file}', [DownloadsController::class, 'download'])->name('download');
Route::get('send-mail', function () {

    $details = [
        'title' => 'Mail from DTS',
        'body' => 'This is for testing email using smtp'
    ];

    \Mail::to('ngoctuyen110208@gmail.com')->send(new \App\Mail\MyMail($details));

    dd("Email is Sent.");
});
Route::get('notification/{id}', [NotificationController::class, 'show'])->name('notification');
