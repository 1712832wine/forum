<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Users') }}
    </h2>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <div class="table-responsive-sm">
                <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Avatar</th>
                            <th scope="col">Role</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $item)
                            <tr>
                                <th scope="row" class="align-middle text-center">{{ $item->id }}</th>
                                <td class="align-middle">{{ $item->name }}</td>
                                <td class="text-center">
                                    <img src="{{ $item->profile_photo_url }}" class="m-auto rounded-circle" width="50"
                                        alt="User" />
                                </td>
                                <td class="align-middle">{{ $item->role }}</td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-success"
                                        wire:click.prevent="openModal({{ $item->id }})">
                                        <i class="fas fa-edit mr-1"></i>Change Role
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                        wire:click="confirmDelete({{ $item->id }})">
                                        <i class="far fa-trash-alt mr-1"></i>Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- MODAL CHANGE ROLE --}}
    <div class="modal fade" id="changeRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Select Role of
                        {{ $user->name }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- RADIO --}}
                    <select class="form-control" wire:model.defer="user.role">
                        <option value="Super Admin">Super Admin</option>
                        <option value="Admin">Admin</option>
                        <option value="User">User</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" wire:click="saveChange()">Save
                        changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
