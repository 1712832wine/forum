<div class="inner-main-body p-2 p-sm-3 forum-content">
    <a href="javascript:history.back()" class="btn btn-light btn-sm mb-3 has-icon">
        <i class="fa fa-arrow-left mr-2"></i>Back
    </a>
    @if ($post)
        <div class="card mb-2">
            <div class="card-body">
                @include('livewire.component.setting')
                <div class="media forum-item">
                    <a href="" class="card-link">
                        <img src="{{ $author->profile_photo_url }}" class="rounded-circle" width="50" alt="User" />
                    </a>
                    <div class="media-body ml-3">
                        <h6 class="mt-1 mb-0">
                            {{ $post['title'] }}
                        </h6>
                        @php
                            $category = App\Models\Category::findOrFail($post->category);
                            $status = App\Models\Status::findOrFail($post->status);
                        @endphp
                        @include('livewire.component.badge', ['title' => $category->title, 'background' =>
                        $category->background, 'color' => $category->color])
                        @include('livewire.component.badge', ['title' => $status->title, 'background' =>
                        $status->background, 'color' => $status->color])
                        @foreach (json_decode($post->tags) as $tag)
                            <span class="badge badge-success">#{{ $tag }}</span>
                        @endforeach
                        <br>
                        <small class="text-secondary">{{ $author->name }}</small>
                        <small class="text-muted ml-2">{{ $post['created_at'] }}</small>

                        <div class="mt-3 mb-1 font-size-sm">
                            {!! $post['content'] !!}
                        </div>
                        <div class="row text-center text-lg-left">
                            @foreach (json_decode($post->images) as $image)
                                <div class="col-lg-3 col-md-4 col-6">
                                    <a class="d-block mb-4 h-100" href="{{ asset('storage') }}/{{ $image }}"
                                        data-fancybox="gallery-{{ $post->id }}" data-caption="Caption #1">
                                        <img class="img-fluid img-thumbnail"
                                            src="{{ asset('storage') }}/{{ $image }}" alt="" />
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <!-- Button trigger modal -->
                        <span class="mr-2" data-toggle="modal" data-target="#listVote">
                            <i class="fas fa-heart" style="color:pink;cursor: pointer;"></i>
                            {{ count(json_decode($post->votetotal)) }}
                        </span>

                        <span>
                            <i class="far fa-comment"></i>
                            {{ $comments->total() }}
                        </span>
                    </div>
                </div>
                <hr>
                @php
                    $vote = json_decode($this->post->votetotal);
                    if (!$vote) {
                        $vote = [];
                    }
                    if (in_array(Auth::id(), $vote)) {
                        $isLiked = true;
                    } else {
                        $isLiked = false;
                    }
                @endphp
                <button type="button" class="btn @if ($isLiked) btn-primary @else btn-outline-primary @endif" wire:click="handleLike()"><i
                        class="far fa-heart mr-1"></i>
                    @if ($isLiked)
                        UnVote
                    @else
                        Vote
                    @endif
                </button>
            </div>
        </div>
    @endif
    @include('livewire.component.comment')
    @include('livewire.component.listvote',['list' => $post->votetotal])
</div>
