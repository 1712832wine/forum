<div class="inner-sidebar">
    {{-- CREATE POST BUTTON --}}
    <div class="inner-sidebar-header justify-content-center">
        <button type="button" class="btn btn-primary d-flex align-items-center" wire:click.prevent="openCreatePost()">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-plus mr-2">
                <line x1="12" y1="5" x2="12" y2="19"></line>
                <line x1="5" y1="12" x2="19" y2="12"></line>
            </svg>
            CREATE NEW POST
        </button>
    </div>
    {{-- SIDEBAR --}}
    <div class="inner-sidebar-body p-0">
        <div class="p-3 h-100" data-simplebar="init">
            <div class="simplebar-wrapper" style="margin: -16px;">
                <div class="simplebar-height-auto-observer-wrapper">
                    <div class="simplebar-height-auto-observer"></div>
                </div>
                @php
                    $statuses = App\Models\Status::all();
                    $categories = App\Models\Category::all();
                @endphp
                <div class="simplebar-mask">
                    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                        <div class="simplebar-content-wrapper" style="height: 100%;">
                            <div class="simplebar-content" style="padding: 16px;">


                                <nav class="nav nav-pills nav-gap-y-1 flex-column">
                                    <div class="px-0 m-0 py-1">
                                        <h4><i class="fa fa-filter mr-1"></i>Filter System </h4>
                                        <hr>
                                    </div>

                                    <label for="status">Status: </label>
                                    <select class="form-control custom-select" id="status" wire:model="status">
                                        <option value="">All</option>
                                        @foreach ($statuses as $status)
                                            <option value="{{ $status->id }}" wire:click='updatedStatus'>
                                                {{ $status->title }}</option>
                                        @endforeach
                                    </select>
                                    <label for="category">Category:</label>
                                    <select class="form-control custom-select" id="category" wire:model="category">
                                        <option value="">All</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" wire:click='updatedCategory'>
                                                {{ $category->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                    {{-- PROFILE --}}
                                    {{-- <hr class="w-100" />
                                    @if (!Auth::check())
                                        <a href="/login" class="btn btn-primary" role="button" aria-pressed="true">
                                            Login
                                        </a>
                                    @endif --}}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
