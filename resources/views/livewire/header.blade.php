<div class="inner-main-header justify-content-between">
    <span class="input-icon input-icon-sm w-auto col-lg-4 px-0">
        <input type="text" class="form-control form-control-sm bg-gray-200 border-gray-200 shadow-none mb-4 mt-4"
            placeholder="Search forum" wire:model.debounce.250="search">
    </span>

    @if (Auth::check())
        <div>
            @php
                $unreadNotifications = Auth::user()->unreadNotifications;
            @endphp
            <div class="btn-group">
                <button type="button" class="btn btn-secondary dropdown-toggle notification-count"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Thông báo ({{ $unreadNotifications->count() }})
                </button>
                <div class="dropdown-menu dropdown-menu-right menu-notification">
                    @foreach ($unreadNotifications as $item)
                        @php
                            $author = App\Models\User::findOrFail($item->data['author']);
                        @endphp
                        <a class="dropdown-item" type="button" href="{{ route('notification', $item->id) }}">
                            <img src="{{ $author->profile_photo_url }}" class="rounded-circle avt" alt="User Image">
                            {{ $author->name }} {{ $item->data['message'] }}</a>
                    @endforeach
                </div>
            </div>
            <div class="btn-group">
                <div id="ex2" class="pull-left image" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if (Auth::user()->role === 'Admin' || Auth::user()->role === 'Super Admin')
                        <span class="my-stack has-badge" @if ($count > 0) data-count="{{ $count }}" @endif>
                            <img src="{{ Auth::user()->profile_photo_url }}" class="rounded-circle avt"
                                alt="User Image">
                        </span>
                    @else
                        <img src="{{ Auth::user()->profile_photo_url }}" class="rounded-circle avt" alt="User Image">
                    @endif

                </div>
                <style>
                    #ex2 .my-stack[data-count]:after {
                        position: absolute;
                        right: 0%;
                        top: 1%;
                        content: attr(data-count);
                        font-size: 45%;
                        padding: .6em;
                        border-radius: 999px;
                        line-height: .75em;
                        color: white;
                        background: rgba(255, 0, 0, 0.85);
                        text-align: center;
                        min-width: 2em;
                        font-weight: bold;
                    }

                    .item {
                        padding: .25rem 1.5rem;
                    }

                </style>
                <div class="dropdown-menu dropdown-menu-right">
                    <h6 class="dropdown-header">Your info</h6>
                    <button class="dropdown-item" disabled type="button">
                        <div class="user-panel info">
                            <div>Name: {{ Auth::user()->name }}</div>
                            <div>Mail: {{ Auth::user()->email }}</div>
                        </div>
                    </button>
                    <div class="dropdown-divider"></div>
                    @if (Auth::user()->role === 'Admin' || Auth::user()->role === 'Super Admin')
                        <div class="item">
                            <a href="/pending_posts" class="btn btn-success d-block mb-2" role="button"
                                aria-pressed="true">
                                Pending Posts ({{ $count }})
                            </a>
                        </div>
                    @endif
                    <div class="item">
                        <a href="/dashboard" class="btn btn-primary d-block mb-2" role="button" aria-pressed="true">
                            Go to Dashboard
                        </a>
                    </div>
                    <div class="item">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit" class="btn btn-danger w-100 item" role="button"
                                aria-pressed="true">Logout</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
                encrypted: true,
                cluster: "ap1"
            });
            var channel = pusher.subscribe('NotificationEvent');
            channel.bind('send-message', function(data) {
                Livewire.emit('refreshNotification');
            });
        </script>
    @else
        <a href="/login">
            <div class="alert alert-warning mb-0">
                <strong>Warning!</strong> You have not login. Press here to login
            </div>
        </a>
    @endif

</div>
<style>
    .avt {
        width: 48px;
        height: 48px;
    }

</style>
