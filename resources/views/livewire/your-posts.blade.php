<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Your posts') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <div class="d-flex w-100 justify-content-between px-3 pt-3">
                <button type="button" class="btn btn-primary d-flex align-items-center"
                    wire:click.prevent="openModal()">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-plus mr-2">
                        <line x1="12" y1="5" x2="12" y2="19"></line>
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                    </svg>
                    CREATE NEW POST
                </button>
            </div>
            {{-- MODAL --}}
            @livewire('modal-post')
            {{-- GET POST --}}
            <div class="inner-main-body p-2 p-sm-3 forum-content show bg-white">
                @foreach ($posts as $post)
                    @php
                        $author = '';
                        if ($post->author) {
                            $author = App\Models\User::findOrFail($post->author);
                        }
                        $comments = App\Models\Comment::where([['belongto', $post->id], ['content', 'like', '%' . $this->search . '%']])->get();
                        $count = count($comments);
                        $category = App\Models\Category::findOrFail($post->category);
                        if ($post->status !== 'Pending') {
                            $status = App\Models\Status::findOrFail($post->status);
                        }
                    @endphp


                    <div class="card mb-2">
                        <div class="card-body p-2 p-sm-3">
                            <div class="media forum-item">
                                <img src="{{ $author->profile_photo_url }}" class="mr-3 rounded-circle" width="50"
                                    alt="User" />
                                <div class="media-body">
                                    <a href="{{ route('post_detail', ['id' => $post['id']]) }}">
                                        <h6 class="text-info">
                                            {{ $post['title'] }}
                                        </h6>
                                    </a>
                                    @if ($post['status'] === 'Pending')
                                        <span class="badge badge-pill badge-warning">Pending...</span>
                                    @else
                                        @include('livewire.component.badge', ['title' => $status->title, 'background' =>
                                        $status->background, 'color' => $status->color])
                                    @endif
                                    @include('livewire.component.badge', ['title' => $category->title, 'background' =>
                                    $category->background, 'color' => $category->color])
                                    @foreach (json_decode($post->tags) as $tag)
                                        <span class="badge badge-success">#{{ $tag }}</span>
                                    @endforeach
                                    <p class="text-secondary">
                                        {!! $post['content'] !!}
                                    </p>
                                    <div class="row text-center text-lg-left">
                                        @foreach (json_decode($post->images) as $image)
                                            @php
                                                $type_images = ['jpeg', 'jpg', 'png', 'gif', 'tiff', 'psd'];
                                                $explode = explode('.', $image);
                                            @endphp
                                            @if (in_array($explode[1], $type_images))
                                                <div class="col-lg-3 col-md-4 col-6 px-1">
                                                    <a class="d-block mb-4 h-100"
                                                        href="{{ asset('storage') }}/{{ $image }}"
                                                        data-fancybox="gallery-{{ $post->id }}"
                                                        data-caption="Caption #1">
                                                        <img class="img-fluid img-thumbnail"
                                                            src="{{ asset('storage') }}/{{ $image }}" alt="" />
                                                    </a>
                                                </div>
                                            @else
                                                <a href="{{ route('download', $image) }}">
                                                    {{ $image }} (Download this file)</a>
                                            @endif
                                        @endforeach
                                    </div>

                                    @if ($author)
                                        <p class="text-muted">{{ $author->name }}
                                            <span class="text-secondary font-weight-bold">created at
                                                {{ explode(' ', $post['created_at'])[0] }}</span>
                                        </p>
                                    @endif

                                </div>
                                <div class="text-muted small text-center align-self-center">
                                    <span class="d-none d-sm-inline-block"><i class="far fa-heart"></i>
                                        {{ count(json_decode($post->votetotal)) }}</span>
                                    <span><i class="far fa-comment ml-2 mr-1"></i>{{ $count }}</span>
                                </div>
                            </div>
                            <div>
                                <button type="button" class="btn btn-warning" wire:click="edit({{ $post->id }})">
                                    <i class="far fa-edit mr-1"></i>Edit post
                                </button>
                                <button type="button" class="btn btn-danger"
                                    wire:click="confirmDelete({{ $post->id }})">
                                    <i class="fas fa-trash-alt mr-1"></i>Delete Post
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>

<style>
    nav .pagination {
        justify-content: flex-end
    }

    a:hover {
        text-decoration: none;
    }

    #comments .media {
        border-top: none;
    }

</style>
