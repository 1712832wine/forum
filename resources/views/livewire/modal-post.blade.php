<div wire:ignore.self class="modal fade" id="createPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            @php
                $categories = App\Models\Category::all();
            @endphp
            <div class="modal-body">
                <form>
                    @csrf
                    <div class="form-group">
                        <label for="title" class="col-form-label">Category:</label>
                        <select class="form-control custom-selected" wire:model.defer="post.category">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                        @error('post.category')
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group" wire:ignore>
                        <label for="tags" class="col-form-label">Tags:</label>
                        <select id="tags" class="select2-tags form-control custom-selected" multiple="multiple"
                            wire:model.defer="post.tags">
                            @php
                                $tags = App\Models\Tag::all();
                            @endphp
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->title }}">{{ $tag->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-form-label">Title:</label>
                        <input type="text" class="form-control" id="title" wire:model.defer="post.title">
                        @error('post.title')
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group" wire:ignore>
                        <label for="content" class="col-form-label">Content:</label>
                        <textarea id="editor" class="form-control" wire:model.defer="post.content" data-note="@this">
                            {!! $post->content !!}
                        </textarea>
                        @error('post.content')
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-form-label">Attach Files:</label>
                        <div id="file" wire:key="images" x-on:remove-images.window="Pone.removeFiles();" wire:ignore
                            x-data x-init="
                                    Pone = FilePond.create($refs.input);
                                    Pone.setOptions({
                                        server: {
                                            process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                                                @this.upload('images', file, load, error, progress)
                                            },
                                            revert: (filename, load) => {
                                                @this.removeUpload('images', filename, load)
                                            },
                                        },
                                    });
                                ">
                            <input type="file" x-ref="input" multiple data-max-file-size="3MB">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title" class="col-form-label">Status:</label>
                        <select class="form-control custom-selected" wire:model.defer="post.sharewith">
                            <option value="PUBLISH">PUBLISH</option>
                            <option value="PRIVATE">PRIVATE</option>
                        </select>
                        @error('post.status')
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Create
                    Post</button>
            </div>
        </div>
    </div>
    <script>
        $('#tags').select2({
            tags: true,
            tokenSeparators: [',', ' '],
            width: '100%',
        });
        $('#tags').on('change', function() {
            @this.set('post.tags', $(this).val());
        });
    </script>
    <script>
        var editor;
        ClassicEditor
            .create(document.querySelector('#editor'), {
                toolbar: [
                    'Heading',
                    'bold',
                    'italic',
                    'bulletedList',
                    'numberedList',
                    'indent',
                    'outdent',
                    'blockQuote',
                    'insertTable',
                    'undo',
                    'redo'
                ],

                filebrowserBrowseUrl: '/file_manager/ckeditor',
                filebrowserImageBrowseUrl: '/file_manager/ckeditor',
                filebrowserFlashBrowseUrl: '/file_manager/ckeditor',
                filebrowserUploadUrl: '/file_manager/ckeditor',
                filebrowserImageUploadUrl: '/file_manager/ckeditor',
                filebrowserFlashUploadUrl: '/file_manager/ckeditor',
            }).then(newEditor => {
                editor = newEditor;
                $('#submit').click(() => {
                    @this.set('post.content', editor.getData());
                    @this.saveAndBack();
                });
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            window.addEventListener('refreshTags', (value) => {
                $('#tags').val(null).trigger('change');
            })
            window.addEventListener('refreshContent', (value) => {
                editor.setData("");
            })
        })
    </script>


</div>
