<div class="inner-main-body p-2 p-sm-3 forum-content show bg-white">
    @if (count($posts) > 0)
        @foreach ($posts as $post)
            @php
                $author = '';
                if ($post->author) {
                    $author = App\Models\User::findOrFail($post->author);
                }
                $comments = App\Models\Comment::where([['belongto', $post->id], ['content', 'like', '%' . $this->search . '%']])->get();
                $category = App\Models\Category::findOrFail($post->category);
                $status = App\Models\Status::findOrFail($post->status);
            @endphp


            <div class="card mb-2">
                <div class="card-body p-2 p-sm-3">
                    @include('livewire.component.setting')
                    {{-- CONTENT --}}
                    <div class="media forum-item">


                        <a href="{{ route('profile', ['id' => $post->author]) }}">
                            <img src="{{ $author->profile_photo_url }}" class="mr-3 rounded-circle" width="50"
                                alt="User" />
                        </a>



                        <div class="media-body">
                            <a href="{{ route('post_detail', ['id' => $post['id']]) }}">
                                <h6 class="text-info mb-0">
                                    {{ $post['title'] }}
                                </h6>
                                @include('livewire.component.badge', ['title' => $category->title, 'background' =>
                                $category->background, 'color' => $category->color])
                                @include('livewire.component.badge', ['title' => $status->title, 'background' =>
                                $status->background, 'color' => $status->color])
                            </a>
                            @foreach (json_decode($post->tags) as $tag)
                                <span class="badge badge-success">#{{ $tag }}</span>
                            @endforeach
                            <p class="text-muted mb-0 small">{{ $author->name }}
                                <span class="text-secondary font-weight-bold">created at
                                    {{ explode(' ', $post['created_at'])[0] }}</span>
                            </p>

                            <p class="text-secondary mb-1 mt-3">
                                {!! $post['content'] !!}
                            </p>
                            {{-- FANCYBOX --}}
                            <div class="row mx-0">
                                @foreach (json_decode($post->images) as $image)
                                    @php
                                        $type_images = ['jpeg', 'jpg', 'png', 'gif', 'tiff', 'psd'];
                                        $explode = explode('.', $image);
                                    @endphp
                                    @if (in_array($explode[1], $type_images))
                                        <div class="col-lg-3 col-md-4 col-6 px-1">
                                            <a class="d-block mb-4 h-100"
                                                href="{{ asset('storage') }}/{{ $image }}"
                                                data-fancybox="gallery-{{ $post->id }}" data-caption="Caption #1">
                                                <img class="img-fluid img-thumbnail"
                                                    src="{{ asset('storage') }}/{{ $image }}" alt="" />
                                            </a>
                                        </div>
                                    @else
                                        <a href="{{ route('download', $image) }}">
                                            {{ $image }} (Download this file)</a>
                                    @endif
                                @endforeach
                            </div>

                            <div class="text-muted big align-self-center">
                                <span class="d-none d-sm-inline-block"><i class="far fa-heart"></i>
                                    {{ count(json_decode($post->votetotal)) }}</span>
                                <span><i class="far fa-comment ml-2 mr-1"></i>{{ count($comments) }}</span>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        @endforeach
        {{ $posts->links() }}
        <style>
            nav .pagination {
                justify-content: flex-end
            }

            a:hover {
                text-decoration: none;
            }

            .dropdown:hover>.dropdown-menu {
                display: block;
            }

            .dropdown>.dropdown-toggle:active {
                /*Without this, clicking will make it sticky*/
                /* pointer-events: none; */
            }

        </style>
    @else
        <div class="jumbotron text-center">
            <p class="lead" style="font-size: 2.5rem">There is no posts here!!!</p>
        </div>

    @endif

</div>
