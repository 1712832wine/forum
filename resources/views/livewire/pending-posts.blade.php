<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Pending Posts') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            {{-- MODAL --}}
            @livewire('modal-post')
            {{-- GET POST --}}
            @if (count($posts) > 0)
                <div class="inner-main-body p-2 p-sm-3 forum-content show bg-white">
                    <h4>Total: {{ $posts->total() }}</h4>
                    @foreach ($posts as $post)
                        @php
                            $author = '';
                            if ($post->author) {
                                $author = App\Models\User::findOrFail($post->author);
                            }
                            $comments = App\Models\Comment::where([['belongto', $post->id], ['content', 'like', '%' . $this->search . '%']])->get();
                            $count = count($comments);
                            $category = App\Models\Category::findOrFail($post->category);
                        @endphp


                        <div class="card mb-2">
                            <div class="card-body p-2 p-sm-3">
                                <div class="media forum-item">
                                    @if ($author)
                                        <img src="{{ $author->profile_photo_url }}" class="mr-3 rounded-circle"
                                            width="50" alt="User" />
                                    @endif

                                    <div class="media-body">
                                        <a href="{{ route('post_detail', ['id' => $post['id']]) }}">
                                            <h6 class="text-info">
                                                {{ $post['title'] }}
                                            </h6>
                                        </a>
                                        <span class="badge badge-pill badge-warning">Pending...</span>
                                        @include('livewire.component.badge', ['title' => $category->title, 'background'
                                        =>
                                        $category->background, 'color' => $category->color])

                                        @foreach (json_decode($post->tags) as $tag)
                                            <span class="badge badge-success">#{{ $tag }}</span>
                                        @endforeach

                                        <p class="text-secondary">
                                            {!! $post['content'] !!}
                                        </p>
                                        <div class="row text-center text-lg-left">
                                            @foreach (json_decode($post->images) as $image)
                                                <div class="col-lg-3 col-md-4 col-6">
                                                    <a class="d-block mb-4 h-100"
                                                        href="{{ asset('storage') }}/{{ $image }}"
                                                        data-fancybox="gallery-{{ $post->id }}"
                                                        data-caption="Caption #1">
                                                        <img class="img-fluid img-thumbnail"
                                                            src="{{ asset('storage') }}/{{ $image }}"
                                                            alt="" />
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>

                                        @if ($author)
                                            <p class="text-muted">{{ $author->name }}
                                                <span class="text-secondary font-weight-bold">created at
                                                    {{ explode(' ', $post['created_at'])[0] }}</span>
                                            </p>
                                        @endif

                                    </div>
                                    <div class="text-muted small text-center align-self-center">
                                        <span class="d-none d-sm-inline-block"><i class="far fa-heart"></i>
                                            {{ count(json_decode($post->votetotal)) }}</span>
                                        <span><i class="far fa-comment ml-2 mr-1"></i>{{ $count }}</span>
                                    </div>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-warning"
                                        wire:click="changeStatus({{ $post->id }})">
                                        <i class="far fa-edit mr-1"></i>Change Status
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                        wire:click="confirmDelete({{ $post->id }})">
                                        <i class="fas fa-trash-alt mr-1"></i>Delete Post
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{ $posts->links() }}
                </div>
            @else
                <div class="jumbotron m-0 text-center">
                    <h2>There is no pending post here!!!</h2>
                </div>
            @endif

        </div>
    </div>
    {{-- MODAL CHANGE STATUS --}}
    <div wire:ignore.self class="modal fade" id="PendingPostModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @csrf
                        @php
                            $statuses = App\Models\Status::all();
                        @endphp
                        <div class="form-group">
                            <label for="title" class="col-form-label">Status:</label>
                            <select class="form-control custom-selected" wire:model.defer="post.status">
                                <option value="Pending">Pending</option>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}">{{ $status->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" wire:click.prevent="saveChange()">Save
                        changes</button>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    nav .pagination {
        justify-content: flex-end
    }

    a:hover {
        text-decoration: none;
    }

    #comments .media {
        border-top: none;
    }

</style>
