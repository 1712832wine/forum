<div class="row profile">
    @php
        $followings = json_decode($user->followings);
        $followers = json_decode($user->followers);
        if (!$followings) {
            $followings = [];
        }
        if (!$followers) {
            $followers = [];
        }
    @endphp
    <div class="col-md-3">
        <div class="profile-sidebar">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic text-center">
                <img src="{{ $user->profile_photo_url }}" class="rounded-circle avt" alt="User Image">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    {{ $user->name }}
                </div>
                <div class="profile-usertitle-job">
                    {{ $user->email }}
                </div>
                <hr>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="">
                            <a href="{{ route('all') }}">
                                <i class="fas fa-newspaper"></i>{{ count($posts) }} Posts
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('all') }}">
                                <i class="fas fa-user-friends"></i>{{ count($followers) }} Followers
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('all') }}">
                                <i class="fas fa-user"></i>{{ count($followings) }} Following
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->
            <!-- SIDEBAR BUTTONS -->
            <div class="profile-userbuttons">
                @if (Auth::id() !== $user->id)
                    @if (in_array(Auth::id(), $followers))
                        <button type="button" class="btn btn-success btn-sm" wire:click="follow()">Unfollow</button>
                    @else
                        <button type="button" class="btn btn-outline-success btn-sm"
                            wire:click="follow()">Follow</button>
                    @endif
                @endif

                <a type="button" class="btn btn-danger btn-sm" href="{{ route('all') }}">
                    Return
                </a>
            </div>
            <!-- END SIDEBAR BUTTONS -->
            <!-- SIDEBAR MENU -->

            <!-- END MENU -->
        </div>
    </div>
    <div class="col-md-9">
        <div class="profile-content">
            @include('livewire.get-posts')
        </div>
    </div>
</div>
