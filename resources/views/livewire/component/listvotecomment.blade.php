<!-- Modal -->
<div class="modal fade" id="listVote1" tabindex="-1" role="dialog" aria-labelledby="exampleListVote" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleListVote">List vote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @foreach ($list as $item)
                    @php
                        $user = App\Models\User::find($item);
                    @endphp
                    <div class="pull-left image mb-2">
                        <img src="{{ $user->profile_photo_url }}" class="rounded-circle avt" alt="User Image">
                        <span>{{ $user->name }}</span>
                    </div>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
