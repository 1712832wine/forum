@if (Auth::check())
    {{-- if logged in --}}
    {{-- isSuperAdmin or isAuthor --}}
    @if (Auth::user()->role === 'Super Admin' || Auth::id() === $post->author)
        <div class="btn-group" style="float:right">
            <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-cog"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <button wire:click.prevent="editPost({{ $post->id }})" class="dropdown-item color-red btn"
                    type="button">
                    <i class="far fa-edit mr-1"></i>Edit this post
                </button>
                <button wire:click.prevent="confirmDelete({{ $post->id }})" class="dropdown-item text-danger btn"
                    type="button">
                    <i class="far fa-trash-alt mr-1"></i>Delete this post
                </button>
            </div>
        </div>
    @endif
@endif
