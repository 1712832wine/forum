@php
$templates = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'];
if (in_array($background, $templates)) {
    $isTemplate = true;
} else {
    $isTemplate = false;
}
@endphp
@if ($isTemplate)
    <span class="badge badge-pill badge-{{ $background }} cursor-pointer" style="
        background: {{ $background }};
        color: {{ $color }};">
        {{ $title }}
    </span>
@else
    <span class="badge badge-pill cursor-pointer" style="
        background: {{ $background }};
        color: {{ $color }};">
        {{ $title }}
    </span>
@endif
