{{-- TAB PANES --}}
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#comments-tab">Comments</a>
    </li>
</ul>
<div class="tab-content">
    <div wire:ignore.self id="comments-tab" class="tab-pane active bg-white pb-3 pt-3 px-3">
        {{-- ADDCMT --}}
        <h3 class="pull-left">New Comment</h3>
        <div class="media">

            <a class="pull-left mr-3" href="#">
                @if (Auth::check())
                    <img class="media-object" src="{{ Auth::user()->profile_photo_url }}" alt="">
                @else
                    <img class="media-object" src="https://via.placeholder.com/64" alt="">
                @endif
            </a>
            <form class="media-body" wire:ignore>
                @csrf
                <textarea class="form-control" rows="5" id="comment" wire:model.defer="comment.content"
                    placeholder="You have to login to comment here."
                    data-note="@this">{!! $comment->content !!}</textarea>
                @error('comment.content')
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ $message }}
                    </div>
                @enderror
                {{-- FILEPOND --}}
                <div class="form-group">
                    <label for="status" class="col-form-label">Attach Files:</label>
                    <div wire:key="images" x-on:remove-images-n.window="Pone2.removeFiles();" wire:ignore x-data x-init="
                                Pone2 = FilePond.create($refs.input);
                                Pone2.setOptions({
                                    server: {
                                        process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                                            @this.upload('images', file, load, error, progress)
                                        },
                                        revert: (filename, load) => {
                                            @this.removeUpload('images', filename, load)
                                        },
                                    },
                                });
                            ">
                        <input type="file" x-ref="input" multiple data-max-file-size="3MB">
                    </div>
                </div>

                {{-- SUBMIT --}}
                <button type="button" class="btn btn-primary" style="float: right;" id="submitCMT">Submit</button>
            </form>
        </div>

        {{-- COMMENTS --}}
        <section class="content-item" id="comments">
            <div class="row px-3">
                @if ($comments->total() > 0)
                    <div class="col-sm-12 px-0">
                        <h3>{{ $comments->total() }} Comments</h3>
                        @foreach ($comments as $comment_item)
                            @php
                                $author = App\Models\User::findOrFail($comment_item['author']);
                            @endphp
                            <div class="bd-top">
                                <div class="float-right">
                                    @if ($comment_item->priority)
                                        <i class="fas fa-thumbtack mr-1 text-primary"
                                            title="This comment has been pinned" style="margin-bottom:-1px"></i>
                                    @endif
                                    @include('livewire.component.settingcmt')
                                </div>
                                <div class="media">
                                    <a class=" pull-left" href="#">
                                        <img class="media-object" src="{{ $author->profile_photo_url }}" alt=""></a>
                                    <div class="media-body">
                                        <h6 class="media-heading d-table">{{ $author->name }}@if ($author->role === 'Admin' || $author->role === 'Super Admin')
                                                <span class="badge badge-pill badge-warning ml-1">Admin</span>
                                            @endif
                                        </h6>

                                        {!! $comment_item['content'] !!}
                                        <div class="row text-center text-lg-left">
                                            @foreach (json_decode($comment_item->images) as $image)
                                                @php
                                                    $type_images = ['jpeg', 'jpg', 'png', 'gif', 'tiff', 'psd'];
                                                    $explode = explode('.', $image);
                                                @endphp
                                                @if (in_array($explode[1], $type_images))
                                                    <div class="col-lg-3 col-md-4 col-6">
                                                        <a class="d-block mb-4 h-100"
                                                            href="{{ asset('storage') }}/{{ $image }}"
                                                            data-fancybox="comment-{{ $comment_item->id }}"
                                                            data-caption="Caption #1">
                                                            <img class="img-fluid img-thumbnail"
                                                                src="{{ asset('storage') }}/{{ $image }}"
                                                                alt="" />
                                                        </a>
                                                    </div>
                                                @else
                                                    <a class="mx-3" href="{{ route('download', $image) }}">
                                                        {{ $image }} (Download this file)</a>
                                                @endif

                                            @endforeach
                                        </div>
                                        <ul class="list-unstyled list-inline media-detail pull-left d-inline-block cmt">
                                            <li>
                                                <i
                                                    class="fa fa-calendar"></i>{{ explode(' ', $comment_item['created_at'])[0] }}
                                            </li>
                                            @php
                                                $vote = json_decode($comment_item->like);
                                                if (in_array(Auth::id(), $vote)) {
                                                    $isLiked = true;
                                                } else {
                                                    $isLiked = false;
                                                }
                                                
                                            @endphp
                                            <li>
                                                <span class="mr-2" style="cursor: pointer"
                                                    wire:click="showLike({{ $comment_item->id }})">
                                                    <i class="fas fa-heart mr-1" @if ($isLiked) style="color:pink" @endif>
                                                    </i>
                                                </span>

                                                <span
                                                    class="mr-2">{{ count(json_decode($comment_item->like)) }}</span>
                                                <span class="like"
                                                    wire:click="handleLikeComment({{ $comment_item->id }})">
                                                    @if ($isLiked)
                                                        Unvote
                                                    @else
                                                        Vote
                                                    @endif
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            @include('livewire.component.listvotecomment',['list' => $this->commentId])
                        @endforeach
                        {{ $comments->links() }}
                    </div>
                @else
                    <div class="jumbotron w-100 text-center mt-3">
                        <h1>There is no comment here!!!</h1>
                    </div>
                @endif
            </div>
        </section>
    </div>
    <script>
        var editorCMT;
        ClassicEditor
            .create(document.querySelector('#comment'), {
                toolbar: [
                    'Heading',
                    'bold',
                    'italic',
                    'bulletedList',
                    'numberedList',
                    'indent',
                    'outdent',
                    'blockQuote',
                    'insertTable',
                    'undo',
                    'redo'
                ],

                filebrowserBrowseUrl: '/file_manager/ckeditor',
                filebrowserImageBrowseUrl: '/file_manager/ckeditor',
                filebrowserFlashBrowseUrl: '/file_manager/ckeditor',
                filebrowserUploadUrl: '/file_manager/ckeditor',
                filebrowserImageUploadUrl: '/file_manager/ckeditor',
                filebrowserFlashUploadUrl: '/file_manager/ckeditor',
            }).then(newEditor => {
                editorCMT = newEditor;
                $('#submitCMT').click(() => {
                    @this.set('comment.content', newEditor.getData());
                    @this.submitComment();
                });
            })
            .catch(error => {
                console.error(error);
            });

        document.addEventListener('DOMContentLoaded', () => {
            window.addEventListener('refreshEditorCMT', (value) => {
                editorCMT.setData("");
            })
        })
    </script>

</div>

<style>
    nav .pagination {
        justify-content: flex-end;
        /* border-top: 1px dashed #dddddd; */
        padding-top: 1rem;
    }

    h3.pull-left {
        font-weight: 400;
        font-size: 20px;
        color: #555555;
        margin: 10px 0 15px;
        padding: 0;
    }

</style>
