@if (Auth::check())
    {{-- if logged in --}}
    {{-- isSuperAdmin or isAuthor --}}
    @if (Auth::user()->role === 'Super Admin' || Auth::user()->role === 'Admin' || Auth::id() === $comment_item->author || Auth::id() === $post->author)
        <div class="btn-group">
            <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-cog"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                {{-- PIN CMT --}}
                {{-- SUPER ADMIN || ADMIN || AUTHOR OF POST --}}
                @if (Auth::user()->role === 'Super Admin' || Auth::user()->role === 'Admin' || Auth::id() === $post->author)
                    <button wire:click.prevent="confirmPinComment({{ $comment_item->id }})" class="dropdown-item btn"
                        type="button">
                        <i class="fas fa-thumbtack mr-1"></i>Pin this comment
                    </button>
                @endif
                {{-- DELETE COMMENT --}}
                {{-- SUPER ADMIN || ADMIN || AUTHOR OF COMMENT || AUTHOR OF POST --}}
                @if (Auth::user()->role === 'Super Admin' || Auth::user()->role === 'Admin' || Auth::id() === $comment_item->author || Auth::id() === $post->author)
                    <button wire:click.prevent="confirmDeleteComment({{ $comment_item->id }})"
                        class="dropdown-item text-danger btn" type="button">
                        <i class="far fa-trash-alt mr-1"></i>Delete this comment
                    </button>
                @endif
            </div>
        </div>
    @endif
@endif
