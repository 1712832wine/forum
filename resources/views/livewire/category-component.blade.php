<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Category') }}
    </h2>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-3">

            <div class="flex justify-content-between align-items-center pb-2">
                <h4 class="d-none d-lg-block m-0">Categories</h4>
            </div>
            {{-- BTN ADD --}}
            <div class="row">
                <div class="col-lg-8 d-flex m-auto p-0 flex-row-reverse pb-2 m-0">
                    <button class="btn btn-primary" wire:click.prevent="toggleCreate()">
                        <i class="far fa-plus-square"></i> Add Category
                    </button>
                </div>
            </div>
            {{-- LIST --}}
            @if (count($categories) > 0)
                <ul class="list-group row">
                    @foreach ($categories as $item)
                        <li class="list-group-item col-lg-8 m-auto d-flex justify-content-between align-items-center">
                            {{ $item['title'] }}
                            <div>
                                <button type="button" class="btn btn-warning"
                                    wire:click.prevent="openEdit({{ $item->id }})">
                                    <i class="fas fa-edit mr-1"></i>Edit
                                </button>
                                <button type="button" class="btn btn-danger"
                                    wire:click="confirmDelete({{ $item->id }})">
                                    <i class="far fa-trash-alt mr-1"></i>Delete
                                </button>
                            </div>

                        </li>
                    @endforeach
                </ul>
            @else
                <div class="row">
                    <div class="col-lg-8 m-auto p-0 pb-2 m-0 w-100 text-center">
                        <div class="jumbotron">
                            <h5>There is no category here, please insert.</h5>
                        </div>
                    </div>
                </div>
            @endif

            {{-- PAGINATION --}}
            <div class="row">
                <div class="col-lg-8 d-flex m-auto p-0 flex-row-reverse pt-2">
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL FORM --}}
    <div wire:ignore.self class="modal fade" id="CategoryModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @csrf
                        <div class="form-group required">
                            <label for="category">Category</label>
                            <input wire:model.lazy="category.title" type="text" class="form-control" id="category"
                                placeholder="New category name">
                            @error('category.title')
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        {{-- BACKGROUND --}}
                        <div class="form-group">
                            <label for="background">Background</label>
                            <input id="background" type="text" class="form-control input-lg"
                                wire:model.defer="category.background" />

                            @error('category.background')
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        {{-- COLOR --}}
                        <div class="form-group">
                            <label for="color">Color</label>
                            <input id="color" type="text" class="form-control input-lg"
                                wire:model.defer="category.color" />

                            @error('category.color')
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        {{-- SAMPLE --}}
                        <div class="form-group">
                            <label for="example mb-0">Sample:</label>
                            <div class="text-center">
                                @foreach ($templates as $index => $item)
                                    <span class="badge badge-pill badge-{{ $item }} cursor-pointer"
                                        wire:click="changeTemplate({{ $index }})">{{ $item }}</span>
                                @endforeach
                            </div>
                        </div>
                        {{-- REVIEW --}}
                        <div class="form-group">
                            <label for="example mb-0">Review:</label>
                            @php
                                if (strpos($this->category->background, 'template:')) {
                                    $isTemplate = true;
                                } else {
                                    $isTemplate = false;
                                }
                            @endphp
                            <div class="text-center">
                                @if ($this->isTemplate($this->category->background))
                                    <span
                                        class="badge badge-pill badge-{{ $this->category->background }} cursor-pointer"
                                        style="
                                    background: {{ $this->category->background }};
                                    color: {{ $this->category->color }};
                                    font-size: 16px">
                                        @if ($this->category->title)
                                            {{ $this->category->title }}
                                        @else
                                            You have not choose title
                                        @endif
                                    </span>
                                @else
                                    <span class="badge badge-pill cursor-pointer" style="
                                    background: {{ $this->category->background }};
                                    color: {{ $this->category->color }};
                                    font-size: 16px">
                                        @if ($this->category->title)
                                            {{ $this->category->title }}
                                        @else
                                            You have not choose title
                                        @endif
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" wire:click.prevent="saveChange()">Save
                        changes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $('#background, #color').colorpicker({
                autoInputFallback: false,
                format: null,
            });
        })
        $('#background').on('change', function() {
            @this.set('category.background', $(this).val());
        });
        $('#color').on('change', function() {
            @this.set('category.color', $(this).val());
        });
    </script>
</div>
