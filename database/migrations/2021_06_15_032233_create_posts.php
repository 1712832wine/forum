<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->integer('author')->nullable();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('category')->nullable();
            $table->text('tags')->nullable();
            $table->text('content')->nullable();
            $table->text('images')->nullable();
            $table->integer('priority')->nullable();
            $table->integer('voteweek')->nullable();
            $table->integer('votemonth')->nullable();
            $table->integer('votetotal')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
