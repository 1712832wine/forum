<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
                $table->id();
                $table->integer('author')->nullable();
                $table->string('belongto')->nullable();
                $table->text('content')->nullable();
                $table->text('images')->nullable();
                $table->integer('like')->nullable();
                $table->integer('dislike')->nullable();
                $table->integer('priority')->nullable();
                $table->string('status')->nullable();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
