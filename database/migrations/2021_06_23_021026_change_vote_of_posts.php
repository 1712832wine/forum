<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeVoteOfPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->longText('voteweek')->change();
            $table->longText('votemonth')->change();
            $table->longText('votetotal')->change();
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->longText('like')->change();
            $table->longText('dislike')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
