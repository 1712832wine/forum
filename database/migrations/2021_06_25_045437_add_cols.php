<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('background')->nullable()->default('primary')->after('title');
            $table->string('color')->nullable()->default('white')->after('background');
        });
        Schema::table('statuses', function (Blueprint $table) {
            $table->string('background')->nullable()->default('primary')->after('title');
            $table->string('color')->nullable()->default('white')->after('background');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('color','background');
        });
        Schema::table('statuses', function (Blueprint $table) {
            $table->dropColumn('color','background');
        });
    }
}
